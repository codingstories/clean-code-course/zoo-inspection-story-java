package com.epam.engx.story.zoo.external;

import java.util.Collection;

public interface InspectionLog {

    void log(Collection<String> statuses);

}
