package com.epam.engx.story.zoo.external;

public interface ImageRecognitionSystem {

    AnimalStatus recognizeAnimalStatus(Animal animal, Image animalImage);

    EnclosureStatus recognizeEnclosureStatus(Enclosure enclosure, Image enclosureImage);
}
