# Zoo Inspection Story
**To read**: https://epa.ms/zoo-inspection-story-java

**Estimated reading time**: 45 minutes

## Story Outline
Read a small story about automated inspection system for zoo which uses
image recognition system to check animal health and safety of visitors.

This story is about violations of clean code principles with a focus on
functional complexity, proper usage of parameters and level of abstraction.

## Story Organization
**Story Branch**: master
> `git checkout master`

**Practical task tag for self-study**: task
> `git checkout task`

Tags: #clean_code, #java, #parameters, #dry